/**
 * Core controla los flujos de trabajo principales del programa
 * 
 **/
$(document).ready(function() {
	//INICIALIZA EL SISTEMA
	work("start",null);

});




//FLUJO DE TRABAJO




/**
 * Funcion principal de trabajo, todas las llamadas al sistema deben pasar por work
 * PARAMS:
 * 		-mode : modo de trabajo, que se solicita al cliente
 **/
function work(mode, data) {
	if(!working) return false;
	switch(mode) {
		case 'start'://INICIAR PANEL
			startPage();
		break;
		case 'login'://INICIAR SESION
			login();
		break;
		case 'logout'://CERRAR SESION
			cerrarsesion();
		break;
		case 'menu': //SOLICITUD DE CAMBIO DE PAGINA
			$("#menu").removeClass("d-block");
      		$("#menu").addClass("d-none");
			menuWork(data);
		break;
		case 'searchFilter':
			filterFunction();
		case 'specific':
			//Para peticiones que deban ser derivadas a JS de los plugins (Sistema no implementado)
		break;
	}
	return false;
}




//INICIALIZACION




/**
 * Llamada para iniciar el panel
 * */
function startPage() {
	startError(startSesion);
}

/**
 * Inicializa el sistema de TimeOut.
 * Esta funcion se ejecutará cada 60 segundos llamando las funciones de los plugins que necesiten ejecutarse
 * de forma repetitiva.
 **/
function startTO() {
	setTimeout(function() {
		if (!working) return false;
		if(actualPosition == "login") return false; 
		wg_update_to.forEach(function(item, index) {
			item();
		});
		startTO();
	}, 60000);
}




//CONTROL DE PLUGINS




/**
 * Función encargada de validar widgets
 * solicita la validacion al server.
 * PARAMS:
 * 	- modul (str): Modulo al que pertenece el widget
 * 	- wg (str): nombre identificativo del widget
 * 	- func (function): funcion a ejecutar en caso de validacion correcta
 * */
function validateWidget(modul, wg, func) {
 		transmit(modul,'validateWidget',wg,true,func);
}

/**
 * Función encargada de validar widgets localmente
 * PARAMS:
 * 	- modul (str): Modulo al que pertenece el widget
 * 	- wg (str): nombre identificativo del widget
 * */
function localValidateWidget(modul, wg) {
	let status = false;
	if(modul in datos.widgets){
		datos.widgets[modul].forEach((item, i) => {
			if(item.name == wg) {
				status = true;
			}
		});
	}
	return status;
}

/**
 * Funcion encargada de cargar los widgets dde la pagina.
 * 
 **/
function loadWidgets() {
	if (mainContentVar.wg_static) {
		if(mainContentVar.wg_data.length == 0) {
			activeWidgets();
			return null;
		}
		wg_loaded = mainContentVar.wg_data.length;
		mainContentVar.wg_data.forEach(function (item, index) {
			if(localValidateWidget(item.module, item.name)) {
				validateWidget(item.module, item.name, function(r) {
					if(r.code == 0) return false;
					datos.modules.forEach(function(mod, i) {
						if(mod.code == item.module) {
							get('pages/plugins/' + mod.file + '/widgets/'+ item.name + '.html', function(d) {
								$("#wg_place_pos").append(d);
								activeWidgets();
							});
						}
					});
				});
			}else {
				activeWidgets();
			}
		});
	} else {
		//Gestion del caso de opciones de cliente
		permissionError();
	}
}

/**
 * Activa los widgets insertados en la pagina
 * */
function activeWidgets() {
	if(wg_loaded < 0) fatalError('wgActivationError');
	if(wg_loaded > 0) wg_loaded = wg_loaded-1;
	if(wg_loaded == 0) {
		if(mainContentVar.wg_data.length > 0){
			let elementos = $("#wg_place_pos");
			let listadoElementos = $("#wg_place_pos").children('div');
			listadoElementos.sort((a,b) => {
				var getPos = (id) => {
					var e = -1;
					mainContentVar.wg_data.forEach(function (item, index) {
						if((item.module + '-' + item.name) == id) e = item.position;
					});
					return e;
				};
				var a_data = getPos(a.getAttribute('wg-id'));
				var b_data = getPos(b.getAttribute('wg-id'));
				if(a_data > b_data) return 1;
				if(a_data < b_data) return -1;
				return 0;
			});
			listadoElementos.detach().appendTo(elementos);
		}
		while(wg_load.length > 0)
		{
			a= wg_load.pop();
			a();
		}
		mainCloseLoad();
	}
}