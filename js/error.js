/**
 * Error se encarga de controlar los errores del cliente
 * 
 * VARIABLES:
 **/




 //INICIALIZACION




/**
 * Inicializa el el control de errores, agregando al HTML los errores generales del panel.
 * */
function startError(fun) {
	get('pages/Error.html', function(data) {
		$("body").prepend(data);
		started = true;
		validateStart();
		fun();
	});
}

/**
 * Comprueba posibles errores heredados en las cookies
 * En caso de existir un error lanza el mensaje requerido
 * */
function validateStart() {
	errorMsg = getCookie("errorMsg");
	switch(errorMsg) {
		case "moduleNotFound":
		case "noPermission"://Errores de permisos, pueden deberse en cambios en los permisos de acceso. Se espera su solucción al actualizar el panel
			showError3(errorMsg);
		break;
		case "incorrectRequest":
		case "notImplementedYet"://Errores fatales del cliente, se desconoce como se han generado, exigen el bloqueo de la ejecucion
			setCookie("errorMsg","",-1);//Cierra cookie
			showClientError(errorMsg);
		break;
	}
}




//VISUALIZACION DE ERRORES



/**
 * Muestra el error como una notificación en pantalla
 **/
function showError3(error) {
	$("#error_index_code_3").text(error);
	$("#error_index_3").fadeIn();
}

/**
 * Muestra el error fatal del servidor, bloquea la ejecución
 * */
function showError2(error) {
	$("#Main").fadeOut(500, function() {
		$("#error_index_fatalServer_code").text(error);
		$("#error_index_fatalServer").fadeIn(500);
		working = false;
	});
}

/**
 * Muestra un error fatal del ciente, bloquea la ejecución y reinicia el puntero del menu (MainPosition)
 **/
function showClientError(error) {
	working=false;
	setCookie("MainPosition","",-1);
	$("#Main").fadeOut(500, function() {
		$("#error_index_fatalCliente_code").text(error);
		$("#error_index_fatalCliente").fadeIn(500);
	});
}




//GESTION DE ERRORES




/**
 * Valida el error producido. Comprueba si el error concuerda con el ultimo error registrado en errorMsg 
 * */
function validateError(error) {
	return getCookie("errorMsg") != error;
}

/**
 * Es llamada cuando se recibe un error de peticion (-3)
 * Comprueba si el error ya se habia cometido en los ultimos 36 segundos.
 * En caso de ser la primera vez, registra el error y reinicia el panel con la esperanza de que sea un error puntual
 * Si ya se habia producido el error, muestra un error fatal que bloquea el proceso.
 * */
function peticionError(response) {
	console.log(response);
	if(validateError(response)) {
		setCookie("errorMsg", response, 0.01);//36 segundos para evitar error
		window.location.reload(true);
	} else {
		setCookie("errorMsg","",-1);//Cierra cookie
		showClientError(response);
	}
}

/**
 * Es llamada cuando se produce un error en el servidor (-2)
 **/
function exceptionalError(response) {
	console.log(response);
	showError2(response);
}

/**
 * Es llamada cuando se produce un error de sesiones (-1)
 * Envia a sesion la peticion de mostrar el login por timeout
 * */
function sessionError(response) {
	$("#loading").fadeIn(500,function() {startLogin(2);});
}

/**
 * Es llamada cuando se genera un error fatal desconocido
 * */
function fatalError(response) {
	console.log(response);
	showClientError('unknownError');
}

/**
 * Es llamada cuando se produce un error en la comunicación.
 * Comprueba si hay conexión, en caso afirmativo lanza un error de servidor
 * En caso de estar sin conexion, muestra un error de conexion.
 * */
function connectError(response) {
	if(started) {
		if(window.navigator.onLine) showError2(response);
		else{
			$("#Main").fadeOut(500, function() {
				$("#error_index_conection").fadeIn(500);
				working=false;
			});
			console.log(response);
		}
	} else {
		working = false;
		$("#error_index_fatalCargaErrores").fadeIn(500);
	}
}

/**
 * ??????????????????????????
 * */
function clienteError(response) {
	showClientError('loginError');
	console.log(response);
}

/**
 * LLamada cuando se detecta un intento de acceder a un recurso no permitido
 * */
function permissionError() {
	console.log("No tienes permiso");
	showClientError('noPermission');
}