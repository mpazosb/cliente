/**
 * Funciones y variables de uso comun
 * VARIABLES:
 * 		- working (bool): Control de errores, si se establece en false, work dejará de resolver peticiones
 * 		- datos (object): Informacion de la sesion entregada por el servidor al iniciar la sesion
 *		- started (bool): Control de carga de errores, si se genera un error siengo false significa que no se han cargado los errores, empleo error de carga.
 * 		- actualPosition (str): Puntero de posicion, indica la pagina que se esta mostrando. inicializado en login
 * 		- cookieTimeOut (int): Establece la duración de las cookies que registran el puntero de posicion.
 * 		- wg_load (array): Almacena las funciones de inicializacion de los widgets
 * 		- wg_update_to (array): Almacena las funciones repetitivas (llamadas cada 60 segundos)
 * 		- mainContentVar (object): Alacena información sobre la página cargada. Se modifica al remplazar el contenido
 **/
var working = true;
var datos;
var started = false;
var actualPosition = 'login';
var cookieTimeOut = 2;
var wg_loaded = 0;
var wg_load = [];
var wg_update_to = [];
var mainContentVar = {
	title: "NordésTPV",//Titulo del contenido
	pageTitle: "NordésTPV", //Titulo de la pagina
	wg_static: true, //Obtiene los widgets de forma estatica (Falta implementacion dinamica)
	wg_data: [] //Widgets de la pagina
};





/**
 * Funciones de apoyo
 */

function formatDateY_M(date) {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	year = d.getFullYear();

	if (month.length < 2) 
	month = '0' + month;
	return [year, month].join('-');
}





/**
 * COOKIES FUNCTIONS
 */
function setCookie(cname, cvalue, exhours) {
	var d = new Date();
	d.setTime(d.getTime() + (exhours * 60 * 60 * 1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}
