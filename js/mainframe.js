/**
 * CONTROLA EL DISEÑO COMUN DE LA PAGINA
 **/




//INICIALIZACION




/**
 * Iicializa la vista del panel
 * PARAMS:
 * 	-data (object): objeto con información entregada por el servidor sobre la sesion (nombre del usuario, modulos accesibles...)
 **/
function startMain(data) {
	datos = data;
	changePosition(getMainPosition());
	addEventListener('popstate', movePage)
	createMain(data);
}

/**
 * Agrega los contenidos comunes a la pagina.
 * Inicia el temporizador de un minuto requerido por los plugins, descarga la plantilla de la página,
 * la inserta en el HTML, establece el nombre del usuario en el navbar y carga el menu y el listado para
 * el buscador.
 * PARAMS:
 * 	-data (object): objeto con información entregada por el servidor sobre la sesion (nombre del usuario, modulos accesibles...)
 **/
function createMain(data) {
	startTO();
	get('pages/main.html', function(d) {
		$("#Main").html(d);
		$("#navbar-name").html(function(index, text) {
			return text.replace("{{name}}",data.name);//ESTABLECE EL NOMBRE DEL USUARIO
		});
		createMenu(data.modules);
		createBusqueda(data.modules);
		$("#loading").fadeOut(300,createContent);
	});
}

/**
 * Inserta en la página el listado de paginas accesibles en el menu
 * PARAMS:
 * 	- modulos (array): listado de modulos accesibles por el usuario
 **/
function createMenu(modulos) {
	get('pages/menu.html', function(d) {
		$("#menu_work").append(d);
		modulos.forEach(function(item, index) {
			get('pages/plugins/' + item.file + '/menu.html', function(d) {
				$("#menu_work").append(d);
				item.plugins.forEach((plugin, i) => {
					$("#menu_"+item.code+"_"+plugin.mode).show();
				});
			});
		});
  	});
}

/**
 * Inserta en la página el listado de paginas accesibles en el buscador
 * PARAMS:
 * 	- modulos (array): listado de modulos accesibles por el usuario
 **/
function createBusqueda(modulos) {
	get('pages/search.html', function(d) {
    	$("#searchDropdown").append(d);
  	});
  	modulos.forEach(function(item, index) {
  		get('pages/plugins/' + item.file + '/search.html', function(d) {
  			$("#searchDropdown").append(d);
  			item.plugins.forEach((plugin, i) => {
				$("#search_"+item.code+"_"+plugin.mode).removeClass("d-none")
			});
  		});
  	});
}

/**
 * Limpia los arrays de funciones de plugins y llama a la funcion que agrega el contenido
 **/
function createContent() {
	wg_load = [];
	wg_update_to = [];
	if(actualPosition == 'home') createHome();
	else{
		createOther();
	}
	
}

/**
 * Introduce en el HTML el contenido de la pagina principal
 **/
function createHome() {//AGREGAR CONTROL WG
	get('pages/home.html', function(d) {
		$("#main-content").html(d);
		$("#main-title").text(mainContentVar.title);
		$(".menu_element").removeClass("active");
		$("#menu_home").addClass("active");
		mainCloseLoad();
	});
}

/**
 * Inptoduce en el HTML el contenido de la pagina
 **/
function createOther() {
	modulo = actualPosition.split("_")[0];
	plugin = actualPosition.split("_")[1];
	moduloFile = null;
	pluginFile = 'main.html';
	try{
		datos.modules.forEach(function(item, index) {
			if(item.code == modulo) {
				moduloFile = item.file;
				try {
					item.plugins.forEach(function(item, index) {
						if(item.mode == plugin) {
							pluginFile = item.file;
							throw Exception;
						}
					});
				} catch(e) {}
				throw Exception;
			}
		});
	}catch(e) {}
	if (moduloFile == null) {//Modulo no accesible por el usuario
		permissionError();
		mainCloseLoad();
		return false;
	}
	get('pages/plugins/' + moduloFile + "/" + pluginFile, function(d) {
		$("#main-content").html(d);
		$("#main-title").text(mainContentVar.title);
		$(".menu_element").removeClass("active");
		$("#menu_" + actualPosition).addClass("active");
		loadWidgets();
	});
}




//CONTROL DE POSICION




/**
 * Permite establecer la página en la que se encuentra el usuario.
 * Almacena la pagina actual.
 * PARAMS:
 * 	- position (str): identificador de la página en la que nos encontramos
 **/
function changePosition(position) {
	actualPosition = position;
	if(position != 'login') {
		setCookie('MainPosition', position, cookieTimeOut);
	}
}

/**
 * Obtiene la posicion de la cookie.
 * Si no hay una posicion registrada, devuelve home
 **/
function getMainPosition() {
	pos = getCookie('MainPosition');
	if(pos == '') pos = 'home';
	return pos;
}

/**
 * Gestiona las solicitudes de cambio de posición
 * por parte del usuario
 * */
function menuWork(seleccion) {
	if(actualPosition.split('_')[0] == seleccion.module) {
		if(seleccion.module == 'home') return false;
		if(seleccion.plugin == actualPosition.split('_')[1]) return false;
	}
	if(seleccion.plugin != null)changePosition(seleccion.module + "_" + seleccion.plugin);
	else changePosition(seleccion.module);
	mainOpenLoad(createContent);	
}

/**
 * Establece la posición del navegador al iniciar el sistema
 **/
function startPosition() {
	p = actualPosition;
	if(actualPosition == "login") p = "home";
	history.replaceState({page: p, data: {}}, "", "#"+p);
}

/**
 * Gestiona el evento del historial, pendiente de nueva funcionalidad
 * como cambios del contenido, llamadas a un work mas especifico (futuro)
 **/
function movePage(evento) {
	if (evento.state != null) {
		changePosition(evento.state.page);
		mainOpenLoad(createContent);
	} else {
		startPosition();
	}
}

/**
 * Gestiona el filtrado del buscador
 **/
function filterFunction() {
	var input, filter, ul, li, a, i, count;
  	input = document.getElementById("searchInput");
  	filter = input.value.toUpperCase();
  	div = document.getElementById("searchDropdown");
  	a = div.getElementsByTagName("a");
  	count = 0;
  	for (i = 0; i < a.length; i++) {
	    txtValue = a[i].textContent || a[i].innerText;
	    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      		a[i].style.display = "";
      		count++;
    	} else {
      		a[i].style.display = "none";
    	}
  	}
  	if(count == 0) {
	    $("#searchNone").removeClass("d-none");
  	} else {
	    $("#searchNone").addClass("d-none");
  	}
}





//CONTROL DE PANTALLA DE CARGA




/**
 * Transicion loading->content
 * Pantalla de carga principal
 * */
function mainCloseLoad() {
	$("#main-load").fadeOut(500);
	$("#main").fadeIn(500);
}

/**
 * Transicion content->loading
 * Pantalla de carga principal
 * */
function mainOpenLoad(fun) {
	$("#main-load").fadeIn(500,function() { fun()});
	$("#main").fadeOut(500);
}

/**
 * Genera una toast de carga
 * */
function loadToast() {
  $("#content-Load").html('<div class="toast" id="toastLoading" data-bs-autohide="false" style="position: sticky; top: 0; right: 0; z-index: 2000;"><div class="toast-header"><div class="spinner-border me-2"></div><strong class="me-auto">Actualizando datos</strong><small id="toastLoading-time">Ahora</small><button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"><span aria-hidden="true"></span></button></div><div class="toast-body">Se ha realizado una petición al servidor para actualizar la información mostrada, a la espera de respuesta.</div></div>');  
}