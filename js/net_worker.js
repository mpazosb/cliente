/**
 * Sistemas para la comunicacion con el servidor
 **/




/**
 * Realiza una peticion POST al servidor gestionando los errores de primer nivel (<0) y errores de comunicacion
 * PARAMS:
 * 	-module (str): modulo al que realiza la solicitud
 * 	-mode (str): codigo de la solicitud, establece el tipo especifico de la peticion para el modulo solicitado
 * 	-dat (object): Datos requeridos por el servidor para resolver la solicitud
 * 	-sync (bool): Indica si la solicitud se debe hacer de forma asincrona
 * 	-func (function): funcion llamada al recibir respuesta. Se entrega la respuesta como parametro.
 **/
function transmit(module, mode, dat, sync, func) {
	$.ajax({
		type: "POST",
		url: "/servidor/Core.php",
		async: sync,
		data: {module: module, mode: mode, data: dat},
		success: function (response) {
			var d;
			try{
				if(!working) return false;
				d = JSON.parse(response);
				switch(d.code) {
					case -3:
						//Peticion incorrecta
						peticionError(d.response);
					break;
					case -2:
						//Error excepcional
						exceptionalError(d.response);
					break;
					case -1:
						//Error Sesión
						sessionError(d.response);
					break;
					default:
						// General
						func(d);
				}
			} catch(error) {
				//Error fatal en el servidor
				console.log(error);
				fatalError(response);
			}
		},
		error: function () {
			//Error de conexión
			connectError('serverDisconnect');
		}
	});
}




/**
 * Realiza una petición GET
 * PARAMS:
 * 	-file (str): URL del archivo solicitado
 * 	-func (function): funcion llamada al recibir respuesta. Se entrega la respuesta como parametro.
 **/
function get(file, func) {
	$.get(file,func).fail(function() {
		connectError('getFailure');
	});
}