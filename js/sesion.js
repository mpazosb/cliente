/**
 * Control de sesiones
 **/




//INICIALIZACION




/**
 * Inicializa el control de sesiones
 */
function startSesion() {
	if(!working) return false;
	transmit("session", "validateSession", null, true, function(response) {
		if(response.code == 1) startMain(response.data);
		else if(response.code == 0) startLogin(1);
		else clienteError(response);
	});
}

/**
 * Carga la pantalla de login
 * PARAMS
 * 	-nuevo (int): indica el motivo de la carga, 0->logout; 1->Nuevo acceso; 2->sesion finalizada por time out
 * */
function startLogin(nuevo) {
	actualPosition = 'login';
	startPosition();
	get('pages/login_page.html', function(data) {
		$("#Main").html(data);
		$("#loading").fadeOut(300);
		switch(nuevo) {
			case 0:
				$("#alert_login_logout").slideDown(300);
			break;
			case 2:
				$("#alert_login_sesionEnd").slideDown(300);
			break;
		}
	});
}




//FUNCIONES DE SESION




/**
 * envia una peticion de login con los datos del formulario
 * */
function login() {
	$("#alert_login_logout").hide();
	$("#loading").fadeIn(100, function() {
		user = $("#inputUser").val();
		pass = $("#inputPassword").val();
		transmit("session","login",{ user: user, pass: pass}, true, function(response){
		validateLogin(response);
		});
	});
}

/**
 * Resuelve la respuesta del servidor a la solicitud de login
 * */
function validateLogin(data) {
	if(data.code == 1) startMain(data.data);
	else {
		$("#inputUser").val("");
		$("#inputPassword").val("");
		$("#loading").fadeOut(300, function() {
			$("#inputUser").focus();
        	$("#alert_login_incorrect").slideDown(200).delay(3000).slideUp(200);
		});
	}
}

/**
 * Envia la peticion de cierre de sesion y carga la pantalla de login
 * */
function cerrarsesion() {
	if(confirm('¿Deseas cerrar tu sesión actual?')){
		transmit("session","logout","",true,function(a) {
			startLogin(0);
		});
	}
}